import requests
from bs4 import BeautifulSoup as bs
import json

response = requests.get('https://www.aa.org/pages/en_US/daily-reflection/')

soup = bs(response.text, 'html.parser')

posts = soup.find_all(class_='daily-reflection-container')

with open('daily-reflection.json', 'w') as file:
    for post in posts:
        title = post.find(class_='daily-reflection-header-title').get_text()
        quote = post.find(class_='daily-reflection-header-content').get_text().replace('\r\n', '')
        page = post.find(class_='daily-reflection-content-title').get_text().replace('\u2014 ', '')
        content = post.find(class_='daily-reflection-content').get_text()
        data = [title, quote, page, content]
        json.dump({
            "title": title,
            "quote": quote,
            "page": page,
            "content": content
        }, file, sort_keys=True, indent=4)
